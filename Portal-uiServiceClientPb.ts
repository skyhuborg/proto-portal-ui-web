/**
 * @fileoverview gRPC-Web generated client stub for ui
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

import {
  AuthReq,
  AuthResp,
  GetNodeStatusReq,
  GetNodeStatusResp} from './portal-ui_pb';

export class UiClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: string; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: string; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoGetNodeStatus = new grpcWeb.AbstractClientBase.MethodInfo(
    GetNodeStatusResp,
    (request: GetNodeStatusReq) => {
      return request.serializeBinary();
    },
    GetNodeStatusResp.deserializeBinary
  );

  getNodeStatus(
    request: GetNodeStatusReq,
    metadata: grpcWeb.Metadata | null): Promise<GetNodeStatusResp>;

  getNodeStatus(
    request: GetNodeStatusReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: GetNodeStatusResp) => void): grpcWeb.ClientReadableStream<GetNodeStatusResp>;

  getNodeStatus(
    request: GetNodeStatusReq,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: GetNodeStatusResp) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        new URL('/ui.Ui/GetNodeStatus', this.hostname_).toString(),
        request,
        metadata || {},
        this.methodInfoGetNodeStatus,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ui.Ui/GetNodeStatus',
    request,
    metadata || {},
    this.methodInfoGetNodeStatus);
  }

  methodInfoAuth = new grpcWeb.AbstractClientBase.MethodInfo(
    AuthResp,
    (request: AuthReq) => {
      return request.serializeBinary();
    },
    AuthResp.deserializeBinary
  );

  auth(
    request: AuthReq,
    metadata: grpcWeb.Metadata | null): Promise<AuthResp>;

  auth(
    request: AuthReq,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: AuthResp) => void): grpcWeb.ClientReadableStream<AuthResp>;

  auth(
    request: AuthReq,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: AuthResp) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        new URL('/ui.Ui/Auth', this.hostname_).toString(),
        request,
        metadata || {},
        this.methodInfoAuth,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ui.Ui/Auth',
    request,
    metadata || {},
    this.methodInfoAuth);
  }

}

