"use strict";
/**
 * @fileoverview gRPC-Web generated client stub for ui
 * @enhanceable
 * @public
 */
exports.__esModule = true;
exports.UiClient = void 0;
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck
var grpcWeb = require("grpc-web");
var portal_ui_pb_1 = require("./portal-ui_pb");
var UiClient = /** @class */ (function () {
    function UiClient(hostname, credentials, options) {
        this.methodInfoGetNodeStatus = new grpcWeb.AbstractClientBase.MethodInfo(portal_ui_pb_1.GetNodeStatusResp, function (request) {
            return request.serializeBinary();
        }, portal_ui_pb_1.GetNodeStatusResp.deserializeBinary);
        this.methodInfoAuth = new grpcWeb.AbstractClientBase.MethodInfo(portal_ui_pb_1.AuthResp, function (request) {
            return request.serializeBinary();
        }, portal_ui_pb_1.AuthResp.deserializeBinary);
        if (!options)
            options = {};
        if (!credentials)
            credentials = {};
        options['format'] = 'text';
        this.client_ = new grpcWeb.GrpcWebClientBase(options);
        this.hostname_ = hostname;
        this.credentials_ = credentials;
        this.options_ = options;
    }
    UiClient.prototype.getNodeStatus = function (request, metadata, callback) {
        if (callback !== undefined) {
            return this.client_.rpcCall(new URL('/ui.Ui/GetNodeStatus', this.hostname_).toString(), request, metadata || {}, this.methodInfoGetNodeStatus, callback);
        }
        return this.client_.unaryCall(this.hostname_ +
            '/ui.Ui/GetNodeStatus', request, metadata || {}, this.methodInfoGetNodeStatus);
    };
    UiClient.prototype.auth = function (request, metadata, callback) {
        if (callback !== undefined) {
            return this.client_.rpcCall(new URL('/ui.Ui/Auth', this.hostname_).toString(), request, metadata || {}, this.methodInfoAuth, callback);
        }
        return this.client_.unaryCall(this.hostname_ +
            '/ui.Ui/Auth', request, metadata || {}, this.methodInfoAuth);
    };
    return UiClient;
}());
exports.UiClient = UiClient;
