import * as jspb from "google-protobuf"

import * as google_protobuf_timestamp_pb from 'google-protobuf/google/protobuf/timestamp_pb';

export class NodeStatus extends jspb.Message {
  getUuid(): string;
  setUuid(value: string): NodeStatus;

  getBuild(): string;
  setBuild(value: string): NodeStatus;

  getName(): string;
  setName(value: string): NodeStatus;

  getTime(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTime(value?: google_protobuf_timestamp_pb.Timestamp): NodeStatus;
  hasTime(): boolean;
  clearTime(): NodeStatus;

  getLat(): number;
  setLat(value: number): NodeStatus;

  getLon(): number;
  setLon(value: number): NodeStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NodeStatus.AsObject;
  static toObject(includeInstance: boolean, msg: NodeStatus): NodeStatus.AsObject;
  static serializeBinaryToWriter(message: NodeStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NodeStatus;
  static deserializeBinaryFromReader(message: NodeStatus, reader: jspb.BinaryReader): NodeStatus;
}

export namespace NodeStatus {
  export type AsObject = {
    uuid: string,
    build: string,
    name: string,
    time?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    lat: number,
    lon: number,
  }
}

export class GetNodeStatusReq extends jspb.Message {
  getPlaceholder(): string;
  setPlaceholder(value: string): GetNodeStatusReq;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNodeStatusReq.AsObject;
  static toObject(includeInstance: boolean, msg: GetNodeStatusReq): GetNodeStatusReq.AsObject;
  static serializeBinaryToWriter(message: GetNodeStatusReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetNodeStatusReq;
  static deserializeBinaryFromReader(message: GetNodeStatusReq, reader: jspb.BinaryReader): GetNodeStatusReq;
}

export namespace GetNodeStatusReq {
  export type AsObject = {
    placeholder: string,
  }
}

export class GetNodeStatusResp extends jspb.Message {
  getStatusList(): Array<NodeStatus>;
  setStatusList(value: Array<NodeStatus>): GetNodeStatusResp;
  clearStatusList(): GetNodeStatusResp;
  addStatus(value?: NodeStatus, index?: number): NodeStatus;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetNodeStatusResp.AsObject;
  static toObject(includeInstance: boolean, msg: GetNodeStatusResp): GetNodeStatusResp.AsObject;
  static serializeBinaryToWriter(message: GetNodeStatusResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetNodeStatusResp;
  static deserializeBinaryFromReader(message: GetNodeStatusResp, reader: jspb.BinaryReader): GetNodeStatusResp;
}

export namespace GetNodeStatusResp {
  export type AsObject = {
    statusList: Array<NodeStatus.AsObject>,
  }
}

export class User extends jspb.Message {
  getUsername(): string;
  setUsername(value: string): User;

  getRoles(): string;
  setRoles(value: string): User;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): User.AsObject;
  static toObject(includeInstance: boolean, msg: User): User.AsObject;
  static serializeBinaryToWriter(message: User, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): User;
  static deserializeBinaryFromReader(message: User, reader: jspb.BinaryReader): User;
}

export namespace User {
  export type AsObject = {
    username: string,
    roles: string,
  }
}

export class AuthReq extends jspb.Message {
  getRequesttype(): AuthType;
  setRequesttype(value: AuthType): AuthReq;

  getUsername(): string;
  setUsername(value: string): AuthReq;

  getPassword(): string;
  setPassword(value: string): AuthReq;

  getToken(): string;
  setToken(value: string): AuthReq;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AuthReq.AsObject;
  static toObject(includeInstance: boolean, msg: AuthReq): AuthReq.AsObject;
  static serializeBinaryToWriter(message: AuthReq, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AuthReq;
  static deserializeBinaryFromReader(message: AuthReq, reader: jspb.BinaryReader): AuthReq;
}

export namespace AuthReq {
  export type AsObject = {
    requesttype: AuthType,
    username: string,
    password: string,
    token: string,
  }
}

export class AuthResp extends jspb.Message {
  getStatus(): string;
  setStatus(value: string): AuthResp;

  getToken(): string;
  setToken(value: string): AuthResp;

  getUser(): User | undefined;
  setUser(value?: User): AuthResp;
  hasUser(): boolean;
  clearUser(): AuthResp;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AuthResp.AsObject;
  static toObject(includeInstance: boolean, msg: AuthResp): AuthResp.AsObject;
  static serializeBinaryToWriter(message: AuthResp, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AuthResp;
  static deserializeBinaryFromReader(message: AuthResp, reader: jspb.BinaryReader): AuthResp;
}

export namespace AuthResp {
  export type AsObject = {
    status: string,
    token: string,
    user?: User.AsObject,
  }
}

export enum AuthType { 
  AUTH_LOGIN = 0,
  AUTH_LOGOUT = 1,
  AUTH_CHECK = 2,
}
